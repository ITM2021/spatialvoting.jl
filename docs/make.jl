using SpatialVoting
using Documenter

DocMeta.setdocmeta!(SpatialVoting, :DocTestSetup, :(using SpatialVoting); recursive=true)

makedocs(;
    modules=[SpatialVoting],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/SpatialVoting.jl/blob/{commit}{path}#{line}",
    sitename="SpatialVoting.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/SpatialVoting.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)

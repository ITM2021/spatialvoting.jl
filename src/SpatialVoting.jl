module SpatialVoting
using DataFrames
using StatsBase: countmap
using SparseArrays
import Base.show
import Base.+

include("SVData.jl")
include("DynamicSVGrid.jl")
include("CompressiveEncodingStats.jl")
include("SVGrid.jl")
include("misc_functions.jl")
include("SVPlots.jl")
include("RLE.jl")
# include("SVMakie.jl")

to_ascii(X) = [Int(Char(c)) for c in join(X,"")]
to_real_string(X) = split(String(UInt8.(X)),"")

# 3x faster than to_ascii.(X)
function to_ascii!(X::Array{Any})
Threads.@threads for i=1:length(X)
            X[i] = to_ascii(X[i])
         end
    return X
end

function to_real_parsed(X)
    str_real = split(String(UInt8.(X)),"") 
    real = []
    for str=str_real
        try
            r = parse(Float64,str)
            r = isinteger(r) ? Int(r) : r
            push!(real,r)
        catch
            push!(real,str)
        end
    end
    return real
end



"""
helper function that will get the x,y point that is the lower left corner of a `SVGrid` or `SVCell`
This is because the plotting packages plot the cell `Shape` objects by the lower left corner.
"""
lower_left_corner(svcell::SVCell) = (x=minimum(corners(svcell)[1]),y=minimum(corners(svcell)[2]))
lower_left_corner(svgrid::SVGrid) = (x=minimum(corners(svgrid)[1]),y=minimum(corners(svgrid)[2]))

export lower_left_corner
export CompressiveEncodingStats,DynamicSVGrid!,DynamicSVGrid
export cell_center, calc_xy, keyval, calc_bin_size, min_max_scaler, get_bin_loc, to_ascii, to_real
export SVGrid, to_ascii!, AllCompressiveEncodingStats!, AllCompressiveEncodingStats, SVData
export SVColorScheme

end # module

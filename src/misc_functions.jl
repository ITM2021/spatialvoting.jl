using StatsBase
using CategoricalArrays
using Base.Threads
using IntervalSets
using Statistics

"""
calculates the keyvals (cell numbers) that are contained within a group 
defined by the groups xinterval,yinterval and mxn of the parent grid
"""
function groupkeys(xinterval::Interval{:closed,:open},yinterval::Interval{:closed,:open},mxn::Tuple)
    return LinearIndices(mxn)[xinterval.left:xinterval.right,yinterval.left:yinterval.right]
end

groupkeys(grp::SVGroup) = groupkeys(grp.xbin,grp.ybin,(length(grp.parent_xrange)-1,length(grp.parent_yrange)-1))

"""
Figures out what the real numeric values are for a given bin,set of bins, or cell.
"""
function realspace(cell::SVCell)
    try
        c_child = current_child(cell.key)
    catch
        x_range = cell.parent_xrange
        y_range = cell.parent_yrange
        return (x=Interval{:closed,:open}(first(x_range),last(x_range)),y=Interval{:clsed,:open}(first(y_range),last(y_range)))
    end
    xy = CartesianIndices((length(cell.parent_xrange)-1,length(cell.parent_yrange)-1))[current_child(cell.key)]
    x = xy[1]
    y = xy[2]
    xreal = realspace(x,cell.parent_xrange)
    yreal = realspace(y,cell.parent_yrange)
    return (x=xreal,y=yreal)
end

realspace(xbin::Interval{:closed,:open}, xrange::Union{StepRange,StepRangeLen,LinRange}) = Interval{:closed,:open}(realspace(xbin.left,xrange).left,realspace(xbin.right,xrange).right)
realspace(bin::Integer,xrange::Union{StepRange,StepRangeLen,LinRange}) = Interval{:closed,:open}(xrange[bin],xrange[bin+1])
realspace(left,right,xrange::Union{StepRange,StepRangeLen,LinRange}) = Interval{:closed,:open}(xrange[left],xrange[right])

"""
Given a `SVCell` figure out what the `x` and `y` ranges were used in its parent `SVGrid`
"""
function extent(cell::SVCell)
    nbins = Integer(ceil(√(cell.count)))
    if nbins < 3
    nbins = 3
    end
    xreal,yreal = realspace(cell)
    xrange = range(xreal.left,xreal.right,length=nbins+1)
    yrange = range(yreal.left,yreal.right,length=nbins+1)
    return (x=xrange,y=yrange)
end

function Statistics.middle(cell::SVCell)
    x,y = realspace(cell)
    return (x=middle(x.left:x.right),y=middle(y.left:y.right))
end

Statistics.middle(grid::SVGrid) = (x=middle(grid.x),y=middle(grid.y))
    
Base.isless(ga::SVGrid,gb::SVGrid) = isless(ga.x,gb.x)

function corners(cell::SVCell)
    p = realspace(cell)
    return [p.x.left,p.x.left,p.x.right,p.x.right],
           [p.y.left,p.y.right,p.y.right,p.y.left]
end

function corners(g::SVGrid)
    return [first(g.x),first(g.x),last(g.x),last(g.x)],
           [first(g.y),last(g.y),last(g.y),first(g.y)]
end

function corners(dysvgrid::DynamicSVGrid)
    grids = DataFrame(collect(keys(dysvgrid)))
    xmin = minimum(grids[!,"1"])
    xmax = maximum(grids[!,"1"])
    ymin = minimum(grids[!,"2"])
    ymax = maximum(grids[!,"2"])
    return [xmin,xmin,xmax,xmax],
           [ymin,ymax,ymax,ymin]
end


function Base.in(p,svgrid::SVGrid)
    x = first(svgrid.x) <= p[1] < last(svgrid.x)
    y = first(svgrid.y) <= p[2] < last(svgrid.y)
    return x == true && y == true
end


"""
return the grid size of the parent `SVGrid` in the form of a `Tuple` 
where the first element is the number of bins in the `x` domain
and the second element is the number of bins in the `y` domain
"""
function parent_grid_size(svcell::SVCell)
    return (length(svcell.parent_xrange)-1,length(svcell.parent_yrange)-1)
end


# don't like this name
cell_number(svcell::SVCell) = parse(Integer,split(svcell.key,".")[end])

export corners,parent_grid_size,cell_number

function Base.in(p,svcell::SVCell)
    mxn = parent_grid_size(svcell)
    xy = CartesianIndices(mxn)[cell_number(svcell)]
    x,y = xy[1],xy[2]
    x = svcell.parent_xrange[x] <= p[1] < svcell.parent_xrange[x+1]
    y = svcell.parent_yrange[y] <= p[2] < svcell.parent_yrange[y+1]
    return x == true && y == true
end

function Base.in(p,grp::SVGroup)
    x_extent = realspace(grp.xbin,grp.parent_xrange)
    y_extent = realspace(grp.ybin,grp.parent_yrange)
    return p[1] in x_extent && p[2] in y_extent
end

"""
Calculate the Linear Index of a given `SVCell`
"""
keyval(xbin::Integer,ybin::Integer,mxn::Tuple{Integer,Integer}) = LinearIndices(mxn)[xbin,ybin]
keyval(xbin::Integer,ybin::Integer,svgrid::SVGrid) = keyval(xbin,ybin,(length(svgrid.x)-1,length(svgrid.y)-1))
keyval(cell::SVCell) = Integer(parse(Float64,split(cell.key,".")[end]))

export groupkeys, realspace, keyval, group_filter

function gridspace_slow(x::Number,xrange::Union{StepRange,StepRangeLen,LinRange})
    hist = fit(Histogram,[x],xrange)
    return findall(x->x==1,hist.weights)[1]
end

"""
calculate the left edge of a bin in an unbounded grid space
"""
gridspace(x::X,cell_size::Number) where {X <: Number}= Integer(floor(big(x)/big(cell_size)))


"""
This is essentially a histogram function that will calculate the bin that any number belongs within some range
"""
function gridspace(x::X,lowerbound::B,upperbound::B,nbins::Integer) where {X <: Number, B <: Number}
    # promote things to Big if they're big
    c = Integer(ceil(((         x -         lowerbound )/(         upperbound -         lowerbound ))*         nbins ))
    if (x >= lowerbound) && (x < upperbound)
        if (nbins*(x-lowerbound)/(upperbound-lowerbound)) % 1 == 0
            return Integer(c+1)
        else
            return Integer(c)
        end
    elseif x == upperbound
        bounds = Interval{:closed,:open}(lowerbound,upperbound)
        throw(BoundsError(x, "$x is not in $bounds"))       
    end
    bounds = Interval{:closed,:open}(lowerbound,upperbound)
    throw(BoundsError(x, "$x is not in $bounds"))       
end

gridspace(x::X,xrange::R) where {X <: Number, R <: AbstractRange} = gridspace(x,minimum(xrange),maximum(xrange),length(xrange)-1)

"""
return a list of indicies given a `SVGroup` and an array of keyvals.
The index of `keyvals` should be the same as your input data to the 
corresponding `SVGrid` the group belongs to
"""
function indicies(group::SVGroup,keyvals::Array)
    record_ixs = []
    grp_keys = groupkeys(group)
    for key ∈ grp_keys
        ixs = findall(x->x==key, keyvals)
        push!(record_ixs, ixs)
    end
    return vcat(record_ixs...)
end

export indicies

"""
Calculate where to start and stop bounding boxes in a 1d list. 
This assumes you pass in a list of 0s and 1s 
xlists looks something like 

`xlist = [Int(sum(grd[i,:])>0) for i in 1:size(grd)[2]]`

_Note: start and stop arrays aren't always going to be the same length_
"""
function calc_start_stop(xlist)
    iflag = 0
    xstart,xend = [], []
    gsize = length(xlist)
    for i ∈ 1:gsize
        if xlist[Integer(i)] == 1 && iflag == 0
            iflag = 1
            push!(xstart,i)
        end
        if xlist[Integer(i)] == 0 && iflag == 1
            iflag = 0
            push!(xend,i)
        end
    end
    if xlist[gsize] == 1 && iflag == 1
        push!(xend,gsize)
    end
    return (start=xstart,stop=xend)
end

function dy_calc_start_stop(xlist)
    iflag = 1
    xstart,xend = [xlist[1]], []
    for i ∈ 2:length(xlist)
        prev_bin = xlist[i-1]+1
        curr_bin = xlist[i]
        if (prev_bin == curr_bin) && iflag == 0
            iflag = 1
            push!(xstart,curr_bin)
        end
        if (prev_bin != curr_bin) && iflag == 1
            iflag = 0
            push!(xend,curr_bin)
        end
    end
    if xlist[end-1] == xlist[end] && iflag == 1
        push!(xend,xlist[end])
    end
    return (start=xstart,stop=xend)
end

"""
Given some MxN Array that is the output of `grid(svgrid) |> Array` 
figure out which rows and which columns have things in them.
In the context of Holger's things this is effectively the first step in 
Shadowing. 
"""
function which_rows_cols_populated(grd::Array)
    xlist = [Integer(sum(grd[i,:])>0) for i in 1:size(grd)[1]]
    ylist = [Integer(sum(grd[:,j])>0) for j in 1:size(grd)[2]]
    return xlist,ylist
end

export which_rows_cols_populated

"""
A Simple function to turn the SVGrid Type into a sparse array that is a 2D Histogram
"""
function grid(grid::Array, xbins,ybins)
    return reshape(map(x-> x==undef ? 0 : x.count, grid), xbins,ybins)
end
grid(grd::SVGrid) = grid(grd.grid,length(grd.x)-1,length(grd.y)-1)

function get_groups(grp_dict::Dict, xbins,ybins)
    hist = spzeros(xbins,ybins)
    for k ∈ keys(grp_dict)
        grp = grp_dict[k]
        grp_cells = groupkeys(grp)
        for c in grp_cells
            hist[c] = k
        end
    end
    return hist
end
get_groups(grd::SVGrid) = get_groups(grd.groups,length(grd.x)-1,length(grd.y)-1)

export get_groups

function min_max_scaler(X,xmin,xmax) 
    if xmin == xmax
        xmin = xmin - 0.001
        xmax = xmax + 0.001
    end
    return (X - xmin) / (xmax - xmin ) 
end

min_max_scaler(X,xmin,xmax,n_bins) = floor((X - xmin) * (n_bins - 1) / (xmax - xmin )) 

"""
A function like `get_grid` that will return a histogram in the form of a sparse array 
where each cell value is the number of unique labels that landed in the cell. 
"""
function get_purity_grid(grid_dict::Dict,xbins,ybins,labels)
    hist = spzeros(length(xbins),length(ybins))
    for k ∈ keys(grid_dict)
        key = keyvals(grid_dict[k])
        cell = length(unique([labels[x] for x in grid_dict[k]]))
        if iy > ybins
            println("WTF!!!! =>>",ybins - iy)
            iy = 1
        end
        hist[ix,iy] = cell
    end
    return hist
end
get_purity_grid(grd::SVGrid,labels) = get_purity_grid(grd.grid,grd.x,grd.y,labels)

function get_label_grid(grd_dict::Dict,keyvals,xbins,ybins,labels)
    hist = spzeros(xbins,ybins)
    for k ∈ keys(grd_dict)
        cell_ix = filter_data(k,keyvals)
        if length(labels[cell_ix]) > 1
            hist[keyval(grd_dict[k])] = 5
        else
            cell = labels[cell_ix]
            hist[keyval(grd_dict[k])] = cell
        end
    end
    return hist
end
get_label_grid(grd::SVGrid,keyvals,labels) = get_label_grid(grd.grid,keyvals,length(grd.x)-1,length(grd.y)-1,labels)

group_filter(group::SVGroup,keyvals::Array) = findall(key->key in groupkeys(group),keyvals)

current_child(k::String) = Integer(parse(Float64,split(k,".")[end]))
current_key(k::Number) = k

function filter_data(key,keyvals,data)
    cell_ix = findall(x->key==x,keyvals)
    cell_data = data[cell_ix]
    return cell_data
end

function filter_data(key,keyvals)
    cell_ix = findall(x->key==x,keyvals)
    return cell_ix
end

function cell(key::String,svgrid::SVGrid)
    Keys = split(key,".")
    g = svgrid
    global g
    for key=1:length(Keys)-1
        k = join(Keys[1:key+1],".")
        g = eval(:(g.grid[$k]))
    end
    return g
end


"""
A function that will go over all the keys in SVData/DynamicSVGrid and prune out the keys/cells
that do not have counts that are above the threshold `thres`. This method is using 3 categories
or levels of thresholding, `:high, :medium, :low`. The real count value that each represents 
is just taking the max cell count - min cell (of a keys SVGrid) count split into 3 categories. 
You can also pass in a real number and use that number to be the cut point.
"""
function threshold(sv::DynamicSVGrid,svdata::SVData;update_svdata=false,thres=:high,key_col="key",thres_col="thres")
    keys_to_keep = []
    # all keys are guaranteed to be SVCells
    ks = collect(keys(svdata;key_col=key_col))
    if typeof(first(sv.grid)[2]) <: SVCell
        if typeof(thres) <: Symbol
            thres_count = eval(Expr(:call,thres,sv))
        else
            thres_count = thres
        end
        for k in keys(sv)
            if sv[k].count > thres_count
                push!(keys_to_keep, sv[k].key)
            end
        end
    else
        Threads.@threads for ix=1:length(ks)
            key = ks[ix]
            gkey = get_parent_keyval(key)
            svgrid = sv[gkey]
            if typeof(thres) <: Symbol
                thres_count = eval(Expr(:call,thres,svgrid))
            else
                thres_count = thres
            end
            svcell = sv[key]
            if typeof(thres) <: Interval
                if svcell.count in thres
                    push!(keys_to_keep,key)
                end
            else
                if svcell.count ≥ thres_count
                    push!(keys_to_keep,key)
                end
            end
        end
    end
    for ix in eachindex(keys_to_keep)
        if !isassigned(keys_to_keep,ix)
            keys_to_keep[ix] = "fail"
        end
    end
    deleteat!(keys_to_keep,keys_to_keep .== "fail")
    if update_svdata == false
        return keys_to_keep
    else
        svdata.v[!,thres_col] = [false for x in 1:size(svdata.v)[1]]
        Threads.@threads for ix=1:size(svdata.v)[1]
            svdata.v[ix,thres_col] = svdata.v[ix,key_col] in keys_to_keep
        end
    end
end

"""
A helper function that can be used to thin a dataset and maintain the overal structure 
of the original dataset. 
"""
function thin(svdata::SVData;x=:RS,y=:SS,nbins=5,xcell_size=1,ycell_size=1,thres=nothing,key_col="key",thres_col="thres",anom_col="is_anomaly")
    dysvgrid = DynamicSVGrid(Dict(), x, y, 0)
    DynamicSVGrid!(dysvgrid,svdata,x,y;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size,key_col=key_col)
    resolve!(dysvgrid,svdata;key_col=key_col)
    thin_data = anomalies(svdata;view=false)
    thin_data = SVData(thin_data,svdata.raw_data_cols,svdata.key_cols,svdata.label_cols)
    thin_data[!,key_col] .= undef
    thin_data[!,anom_col] = [false for x in 1:size(thin_data.v)[1]]
    dysvgrid = DynamicSVGrid(Dict(), x, y, 0)
    DynamicSVGrid!(dysvgrid,thin_data,x,y;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size,key_col=key_col)
    resolve!(dysvgrid,thin_data)
    if thres == nothing
        return thin_data 
    else
        threshold(dysvgrid,thin_data;thres=thres,update_svdata=true)
        thin_data = filter(row-> row[thres_col] == true, thin_data.v)
        thin_data = SVData(thin_data,svdata.raw_data_cols,svdata.key_cols,svdata.label_cols)
        thin_data[!,key_col] .= undef
        thin_data = anomalies(thin_data;view=false,anom_col=anom_col)
        thin_data = SVData(thin_data,svdata.raw_data_cols,svdata.key_cols,svdata.label_cols)
        thin_data[!,key_col] .= undef
        return thin_data
    end
end


"""
Super simple function that takes some keyval `String` and will give you back one level grid up
Basically use this if you want to get the parent grid of some terminal leafs. 
"""
get_parent_keyval(keyval::String) = join(split(keyval,".")[1:end-1],".")

"""
helper function for calculating how to break up the cell counts into high medium and low
# TODO handle when all cells have the same count
"""
count_range(sv::Union{DynamicSVGrid,SVGrid}) = minimum(sv):(abs(maximum(sv) - minimum(sv)) / 3):maximum(sv)
high(sv::Union{DynamicSVGrid,SVGrid}) = count_range(sv)[3]
medium(sv::Union{DynamicSVGrid,SVGrid}) = count_range(sv)[2]
low(sv::Union{DynamicSVGrid,SVGrid}) = count_range(sv)[1]

export high,medium,low, threshold, dy_calc_start_stop

using Revise
using CSV, DataFrames, JSON3
# using Dash, DashHtmlComponents, DashCoreComponents
# using Plots, SplitApplyCombine
using SpatialVoting
using LinearAlgebra, Statistics
using IntervalSets
using RDatasets
using BenchmarkTools
using DataFrames
using Distributions
# plotly()
# make some hold out set
data = dataset("datasets", "iris")
g = BetaBinomial(8,443.566,354.6)
g1 = BetaBinomial(4,2.43^32,55.9)
npoints = 20_000
data = DataFrame(Dict(
    "col1" => vcat(rand(g1,Int(0.5npoints)).^4/5,rand(g,Int(0.5npoints))),
    "col2" => rand(g,npoints).^23, 
    "col3" => rand(npoints) .* π .* log(4), 
    "col4" => collect(1:npoints) .* log(rand(1)[1]) .* π .* -1.34, 
    "col5" => rand(npoints) .* log(9) / 445
    )
    )

# more realistic looking data I found.. ok its real data.. but whatever..
using CSV
data = CSV.read("/home/mcamp/Downloads/hygdata_v3.csv",DataFrame)
cols = ["hd", "hr", "ra", "dec", "dist", "pmra", "pmdec", "rv", "mag", "absmag", "ci", "x", "y", "z", "vx", "vy", "vz", "rarad", "decrad", "pmrarad", "pmdecrad", "flam", "comp", "comp_primary", "lum", "var_min", "var_max"]
data = data[!,cols]
[replace!(data[!,col], missing => 0) for col in names(data)]


sv_x = :TS
sv_y = :SS
# sv_x = :SepalLength
# sv_y = :PetalLength
# sv_x = :col2
rx = names(data)[1]
ry = names(data)[end]
nbins = 5
xcell_size = 4200
ycell_size = 1400
x_resolved_cell_size = 0.5
max_cell_bins = 10
# xcell_size = max_cell_bins * x_resolved_cell_size
# svdata = convert_to_sv(data;label_cols=["Species"])
svdata = convert_to_sv(data;ascii=false,label_cols=[])

dysvgrid = DynamicSVGrid(Dict(), sv_x, sv_y, 0)
DynamicSVGrid!(dysvgrid,svdata,sv_x,sv_y;xbins=max_cell_bins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size,auto_resolve=true)
grid_plot = plot(dysvgrid;plot_grids=false,aspect_ratio=:equal);
grid_data = Plots.plotly_series(grid_plot)
grid_layout = Plots.plotly_layout(grid_plot)
scatter_data = anomalies(svdata)
raw_plot = plot(scatter_data[!,rx],scatter_data[!,ry])
# TODO: figure out how to make this be local
app = dash(external_stylesheets=["https://codepen.io/chriddyp/pen/bWLwgP.css"])
# app = dash(external_stylesheets=["/assets/custom.css"])
# app = dash()

app.layout = html_div(className="row") do
    html_div(
         [
          html_div(children=[
                             dcc_input(id="xcell_size", value="0.75", type="text"),
                             dcc_input(id="ycell_size", value="0.75", type="text"),
                             dcc_input(id="xbins", value="5", type="text"),
                             dcc_input(id="ybins", value="5", type="text"),
                            ], 
                   className="row"
                  ),
          html_div(children=[], className="row"),
          html_div(children=[], className="row"),
          html_div(children=[
                             html_div(children=[
                                                html_div(
                                                         dcc_graph(
                                                                   id="level_one_graph", 
                                                                   figure=(data = data, layout = layout)
                                                                  ), 
                                                         className="row"
                                                        ),
                                                html_div(
                                                         children=[
                                                                   dcc_dropdown(
                                                                                ;id="xaxis-column",options=[
                                                                                                            (label = "RM", value = "RM"), 
                                                                                                            (label = "RS", value = "RS"),
                                                                                                            (label = "SS", value = "SS"), 
                                                                                                            (label = "SM", value = "SM"),
                                                                                                            (label = "TM", value = "TM")
                                                                                                           ],
                                                                                value="RM"
                                                                               ),
                                                                   dcc_dropdown(
                                                                                ;id="yaxis-column",options=[
                                                                                                            (label = "RM", value = "RM"), 
                                                                                                            (label = "RS", value = "RS"),
                                                                                                            (label = "SS", value = "SS"), 
                                                                                                            (label = "SM", value = "SM"),
                                                                                                            (label = "TM", value = "TM")
                                                                                                           ],
                                                                                value="SS"
                                                                               )
                                                                  ], 
                                                         className="row"
                                                        )
                                               ],
                                      className="five columns"
                                     ),
                             html_div(
                                      children=[
                                                 html_button(
                                                             "Auto Resolve", 
                                                             id="btn-resolve", 
                                                             n_clicks=0
                                                            ),
                                                 html_button(
                                                             "Resolve Selected",
                                                             id="btn-resolve-cell",
                                                             n_clicks=0
                                                            ),
                                                 html_button(
                                                             "Back",
                                                             id="btn-back-cell",
                                                             n_clicks=0
                                                            ),
                                                ], 
                                      className="one columns"
                                     ),
                             html_div(
                                      children=[
                                                html_div(
                                                         dcc_graph(
                                                                  id="scatter_raw_data"
                                                                 ), 
                                                         className="row"
                                                       ),
                                                html_div(
                                                        children=[
                                                                  dcc_dropdown(;
                                                                               id="xaxis-raw-column",
                                                                               options=[(label = col, value = col) for col in svdata.raw_data_cols],
                                                                               value=svdata.raw_data_cols[1]
                                                                              ),
                                                                  dcc_dropdown(;
                                                                               id="yaxis-raw-column",
                                                                               options=[(label = col, value = col) for col in svdata.raw_data_cols],
                                                                               value=svdata.raw_data_cols[2]
                                                                              )
                                                                 ], 
                                                        className="row"
                                                       )
                                                  ],
                                      className="five columns"
                                     )
                            ]
                  )
         ],
         className="row"
        )
end



# TODO: Callback to resolve cells in second plot
# TODO: Add color / labels to the scatter plot points


callback!(
    app,
    Output("level_one_graph", "figure"),
    Input("xaxis-column", "value"),
    Input("yaxis-column", "value"),
    Input("xbins", "value"),
    Input("ybins", "value"),
    Input("xcell_size", "value"),
    Input("ycell_size", "value"),
    Input("level_one_graph", "hoverData")
) do new_x_stat, new_y_stat, new_xbins, new_ybins, new_xsize, new_ysize, hover_data
    # isempty(callback_context().triggered) && throw(PreventUpdate())
    changed_id = [p[:prop_id] for p in callback_context().triggered][1]

    resvgrid = false
    if occursin("xbins", changed_id)
        resvgrid = true
    end
    if occursin("ybins", changed_id)
        resvgrid = true
    end
    if occursin("xcell_size", changed_id)
        resvgrid = true
    end
    if occursin("ycell_size", changed_id)
        resvgrid = true
    end
    if occursin("xaxis-column", changed_id)
        resvgrid = true
    end
    if occursin("yaxis-column", changed_id)
        resvgrid = true
    end

    if resvgrid == true
        new_xbins = parse(Int, new_xbins)
        new_ybins = parse(Int, new_ybins)
        new_xsize = parse(Float64, new_xsize)
        new_ysize = parse(Float64, new_ysize)
        global x = Symbol(new_x_stat)
        global y = Symbol(new_y_stat)
        global dysvgrid = DynamicSVGrid(Dict(), x, y, 0)
        DynamicSVGrid!(dysvgrid,svdata,x,y;xbins=new_xbins,
                                       ybins=new_ybins,
                                       xcell_size=new_xsize,
                                       ycell_size=new_ysize)
    end
    p = plot(dysvgrid;plot_grids=false,aspect_ratio=:equal);
    xp, yp = hover_data.points[1].x, hover_data.points[1].y
    dcell_size = cell_size(dysvgrid)
    xp, yp = Int(floor(xp / dcell_size.x)), Int(floor(yp / dcell_size.y))
    if (xp, yp) in keys(dysvgrid.grid)
        svgrid = dysvgrid[xp,yp]
        # plot!(svgrid);
        plot!(svgrid;markerstrokewidth=10)
    end
    data = Plots.plotly_series(p)
    layout = Plots.plotly_layout(p)
    return (data = data, layout = layout)
end

# callback!(
#     app,
#     Output("next_level_graph", "figure"),
#     Input("level_one_graph", "clickData"),
# ) do click_data,hover_data

#     dcell_size = cell_size(dysvgrid)

#     if click_data ≠ nothing
#     # @show click_data, hover_data
#         xp, yp = click_data.points[1].x, click_data.points[1].y
#         xp, yp = Int(floor(xp / dcell_size.x)), Int(floor(yp / dcell_size.y))
#         if (xp,yp) in keys(dysvgrid.grid)
#             svcell = dysvgrid[xp,yp]
#             # next_level_graph = svgrid_scatter(dysvgrid,svdata,xp,yp;hover=svdata.raw_data_cols)
#             # plot!(svcell;colors=SpatialVoting.SVColorScheme([:black,:white],[1,2]));
#             next_level_graph = plot(svcell);
#             n_data = Plots.plotly_series(next_level_graph)
#             n_layout = Plots.plotly_layout(next_level_graph)
#             return (data = n_data, layout = n_layout)
#         end
#     end

#     @show hover_data
#     if hover_data ≠ nothing
#         @show "You are hovering"
#         hxp, hyp = hover_data.points[1].x, hover_data.points[1].y
#         hxp, hyp = Int(floor(hxp / dcell_size.x)), Int(floor(hyp / dcell_size.y))
#         hover_key = infer_keyval(dysvgrid,keys(svdata;view=true,unique_set=false)|>Array,hxp,hyp)
#         @show hover_key
#         hover_cell = dysvgrid[hover_key]
#         hp = plot!(hover_cell)
#         plot!(hover_cell;colors=SpatialVoting.SVColorScheme([:black,:white],[1,2]));
#         n_data = Plots.plotly_series(hp)
#         n_layout = Plots.plotly_layout(hp)
#         return (data = n_data, layout = n_layout)
#     end


#     # if hover_key ≠ "anomaly"
#         # @show hover_key
#         # sv = dysvgrid[hxp,hyp]
#         # plot!(sv;colors=SpatialVoting.SVColorScheme([:black,:white],[1,2]));
#     # end
#             return (data = n_data, layout = n_layout)

# end

callback!(
    app,
    Output("btn-resolve", "n_clicks"),
    Input("btn-resolve", "value"),
) do click_data
    # resolve!(dysvgrid,svdata)
    @show "resolved!"
end

# callback!(
#     app,
#     Output("scatter_raw_data", "figure"),
#     Input("next_level_graph", "hoverData"),
#     Input("xaxis-raw-column", "value"),
#     Input("yaxis-raw-column", "value"),

# # ) do click_data
# ) do click_data,raw_x,raw_y

#     xp, yp = click_data.points[1].x, click_data.points[1].y
#     # @show x,y
#     dcell_size = cell_size(dysvgrid)
#     ks = keys(svdata,unique_set=true) |> Array
#     keyclick = infer_keyval(dysvgrid,ks,xp,yp)
#     # @show keyclick
#     cell_data = filter(row-> row["key"] == keyclick,svdata.v)
#     # @show cell_data
#     raw_scatter = plot(cell_data[!, raw_x],cell_data[!, raw_y],seriestype=:scatter,aspect_ratio=:equal)

#     # @show raw_scatter
#     gdata = Plots.plotly_series(raw_scatter) 
#     glayout = Plots.plotly_layout(raw_scatter)
#     return (data = gdata, layout = glayout)
# end

run_server(app, "0.0.0.0", debug=true)


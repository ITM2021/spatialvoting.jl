using Revise
using SpatialVoting
using LinearAlgebra, Statistics
using IntervalSets
using RDatasets
using Test
using BenchmarkTools
using DataFrames

function get_data(;labels::Bool=false)
    iris = dataset("datasets", "iris")
    sort!(iris)
    data = iris[!,1:4] |> Array
    data = collect(eachrow(data))
    if labels==true
        return iris.Species
    end
    return data
end

@testset "SVColorScheme" begin
    svcolor = SVColorScheme()
    @test svcolor[0] == :black
    @test svcolor[1] == :blue
    @test svcolor[4] == :grey
    @test svcolor[8] == :yellow
    @test svcolor[10000] == :red
    @test svcolor[-1] == :black
    @test svcolor[4.43] == :grey
end

# Need someone to make sure these tests are correct. 
# This needs work because source papers aren't exactly 100% correct
@testset "CompressiveEncodingStats" begin
    X = [1, 2, 3]
    ces = AllCompressiveEncodingStats(X;digits=4)
    @test ces.RM == Float64(2.25)
    @test ces.RS == Float64(0.4785)
    @test ces.SM == Float64(1.667)
    @test ces.SS == Float64(0.5884)
    @test ces.TM == Float64(2.25)
    @test ces.TS == Float64(0.4785)
end

@testset "misc" begin
    # if this test fails it means your Julia wasn't compiled correctly.
    @test Float16(8.8) ≈ Float32(8.8)
    X = 51.62
    xrange = 0:17:255 
    bin = gridspace(X,xrange)
    real = realspace(bin,xrange)
    @test X in real

    X = 67.49999999999999999999999999999999
    xrange = 0:17:255 
    bin = gridspace(X,xrange)
    real = realspace(bin,xrange)
    @test X in real

    X = 170
    xrange = 0:17:255 
    bin = gridspace(X,xrange)
    real = realspace(bin,xrange)
    @test X in real

    xrange = 3.423611111111111:0.057388117283950615:4.112268518518518
    bin = gridspace(3.745,xrange)
    real = realspace(Interval{:closed,:open}(1,6),xrange)

    real = realspace(6, 7, 2.5185185185185186:0.20987654320987653:3.7777777777777777)
        
    xrange = 0:17:255
    for x=1:1.3:255
        bin = gridspace(x,xrange)
        real = realspace(bin,xrange)
   @test x in real
        bin = gridspace(Float32(x),xrange)
        real = realspace(bin,xrange)
   @test x in real
        bin = gridspace(Float16(x),xrange)
        real = realspace(bin,xrange)
    @test x in real
    end

    @btime gridspace($X,$xrange)

    group = SVGroup(Interval{:closed,:open}(3,5), Interval{:closed,:open}(1,2),0:17:255,0:17:255,13,13)

end

@testset "keyval" begin
    xsize = 43
    ysize = 250
    xbin = 40
    ybin = 52
    test_grid = reshape(collect(1:1:xsize*ysize),(xsize,ysize));
    key = keyval(xbin,ybin,(xsize,ysize));
    @test test_grid[xbin,ybin] == key

    xsize = 250
    ysize = 250
    xbin = 4
    ybin = 5
    test_grid = reshape(collect(1:1:xsize*ysize),(xsize,ysize));
    key = keyval(xbin,ybin,(xsize,ysize));
    @test test_grid[xbin,ybin] == key

    cell = SVCell(
                   [53, 46, 56, 44, 50, 46, 55, 44, 53, 46, 49, 44, 49, 46, 57],
                   nothing,
                   "0.1",
                   0:16:255,
                   0:16:255,
                   1
                   )
    key = keyval(cell)
    @test key == 1
end



@testset "SVGrid with malware data from Kagal" begin

    using CSV
    data = CSV.read("/home/mcamp/code-home/spatialvoting.jl/dataset.csv", DataFrame) 
    data = convert_to_sv(data; label_cols=["LABEL"])
    x,y = :RM,:SS
# @time    svgrid,keyvals,anomalies = SVGrid(df,data,x,y)
    dysvgrid,keyvals,anomalies = DynamicSVGrid(Dict(),x,y,0),[],[]
    nbins = 5
@time DynamicSVGrid!(dysvgrid,df,data,x,y,keyvals,anomalies;xbins=nbins,ybins=nbins,xcell_size=110,ycell_size=1000)
    [resolve!(dysvgrid.grid[k],df,data,keyvals,anomalies,x,y;prev_key="$(k[1])-$(k[2])") for k in keys(dysvgrid.grid)]

end

@testset "Test SVData" begin
    using CSV
    using Plots
    using Random
    Random.seed!(811);
    # get data
    data = get_data()
    x,y = :RM,:SS
    # make some hold out set
    ix = findall(x-> x ≤ 0.35, rand(length(data)))
    data = dataset("datasets", "iris")
    dont_touch = data[ix,names(data)]
    svdata = convert_to_sv(data;label_cols=["Species"])

end

@testset "Test Basic DynamicSVGrid" begin

    using CSV
    using Plots
    using Random
    Random.seed!(811);
    # get data
    data = get_data()
    x,y = :RM,:SepalWidth
    # make some hold out set
    ix = findall(x-> x ≤ 0.35, rand(length(data)))
    data = dataset("datasets", "iris")
    dont_touch = data[ix,names(data)]
    svdata = convert_to_sv(data;label_cols=["Species"])
    dysvgrid = DynamicSVGrid(Dict(),x,y,0)
    DynamicSVGrid!(dysvgrid,svdata,x,y)

end



@testset "SVGrid check how pure things get" begin

    using CSV
    using Random
    Random.seed!(811);
    # get data
    data = get_data()
    x,y = :RM,:SS

    # make some hold out set
    ix = findall(x-> x ≤ 0.35, rand(length(data)))
    dont_touch = data[ix]
    deleteat!(data,ix)
    
    # get the labels
    labels = get_data(labels=true)
    dt_labels = labels[ix]
    deleteat!(labels,ix)
    
    colnames = [:SepalLength,:SepalWidth,:PetalLength,:PetalWidth]
    data = DataFrame(hcat(data...)', colnames)
    data[:,"Species"] = labels
    # convert into usable forms
    svdata = convert_to_sv(data;label_cols=["Species"])

    dysvgrid_old,keyvals,anomaliez = DynamicSVGrid(Dict(),x,y,0),[],[]
    nbins = 5
    xcell_size = 1
    ycell_size = 1
@time DynamicSVGrid!(dysvgrid_old,df,data,x,y,keyvals,anomaliez;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size)
    # resolve to get all anomalies
    ukeys = length(unique(keyvals))
    done = false
    while done == false
        println("Resolving Data: Unique Keys: $ukeys")
        [resolve!(dysvgrid.grid[k],df,data,keyvals,anomalies,x,y;prev_key="$(k[1])-$(k[2])",xbins=nbins,ybins=nbins) for k in keys(dysvgrid.grid)]
        current_ukeys = length(unique(keyvals))
        if ukeys == length(unique(keyvals))
            done = true
        end
        ukeys = current_ukeys
    end

    # reform anomalies
    data = [convert(Array,a) for a in unique(anomalies)]
    df = copy(data)
    df = to_ascii.(df)
@time    df = AllCompressiveEncodingStats.(df)
@time    df = DataFrame(df)
    nbins = 5
    # make new grid with only anomalies
    dysvgrid,keyvals,anomalies = DynamicSVGrid(Dict(),x,y,0),[],[]
@time DynamicSVGrid!(dysvgrid,df,data,x,y,keyvals,anomalies;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size)
    
    # resolve anomalies

    done = false
    while done == false
        println("Resolving Data: Unique Keys: $ukeys")
        [resolve!(dysvgrid.grid[k],df,data,keyvals,anomalies,x,y;prev_key="$(k[1])-$(k[2])",xbins=nbins,ybins=nbins) for k in keys(dysvgrid.grid)]
        current_ukeys = length(unique(keyvals))
        if ukeys == length(unique(keyvals))
            done = true
        end
        ukeys = current_ukeys
    end

    data = get_data()

    # make some hold out set
    ix = findall(x-> x ≤ 0.2, rand(length(data)))
    dont_touch = data[ix]
    deleteat!(data,ix)
    
    # get the labels
    labels = get_data(labels=true)
    dt_labels = labels[ix]
    deleteat!(labels,ix)
    
    colnames = [:SepalLength,:SepalWidth,:PetalLength,:PetalWidth]
    # convert into usable forms
    data = data |> Array
    data = [convert(Array,Array(c)[1]) for c in eachrow(data)]
    data = convert(Array{Any},data)
    df = copy(data)
    to_ascii!(df)
@time    AllCompressiveEncodingStats!(df)
@time    df = DataFrame(df)
    keyvals,anomalies = [],[]
@time DynamicSVGrid!(dysvgrid,df,data,x,y,keyvals,anomalies;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size)

[(length(unique(x.labels)),unique(x.labels),unique(x.keys)) for x in groupby(lblkeys,[:keys])]


    purity = DataFrame(Dict("keys" => keyvals, "labels" => labels))

    nbins = 5
    df = copy(data)
    df = to_ascii.(df)
@time    df = AllCompressiveEncodingStats.(df)
@time    df = DataFrame(df)
    x,y = :TM,:SS
    dysvgrid,keyvals,anomalies = DynamicSVGrid(Dict(),x,y,0),[],[]

end

@testset "SVGrid test that the grid created with just anomalies is the same" begin

    using CSV
    data = get_data()
    labels = get_data(labels=true)
    colnames = [:SepalLength,:SepalWidth,:PetalLength,:PetalWidth]
    data = data |> Array
    data = [Array(c) for c in eachrow(data)]
    data = convert(Array{Any},data)
    df = copy(data)
    to_ascii!(df)
@time    AllCompressiveEncodingStats!(df)
@time    df = DataFrame(df)
    x,y = :RM,:TS
# @time    svgrid,keyvals,anomalies = SVGrid(df,data,x,y)
    dysvgrid,keyvals,anomalies = DynamicSVGrid(Dict(),x,y,0),[],[]
    nbins = 3
    xcell_size = 0.5
    ycell_size = 0.5
@time DynamicSVGrid!(dysvgrid,df,data,x,y,keyvals,anomalies;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size)
    nbins = nothing
    ukeys = length(unique(keyvals))
    done = false
    while done == false
        println("Resolving Data: Unique Keys: $ukeys")
        [resolve!(dysvgrid.grid[k],df,data,keyvals,anomalies,x,y;prev_key="$(k[1])-$(k[2])",xbins=nbins,ybins=nbins) for k in keys(dysvgrid.grid)]
        current_ukeys = length(unique(keyvals))
        if ukeys == length(unique(keyvals))
            done = true
        end
        ukeys = current_ukeys
    end
    purity = DataFrame(Dict("keys" => keyvals, "labels" => labels))

    anoms = DataFrame(vcat(permutedims.(vcat(anomalies...))...),:auto)
    rename!(anoms, colnames)
    anom_data = anoms |> Array
    anom_data = [Array(c) for c in eachrow(anom_data)]
    anom_data = convert(Array{Any},anom_data)
    anom_df = copy(anom_data)
    to_ascii!(anom_df)
@time    AllCompressiveEncodingStats!(anom_df)
@time    anom_df = DataFrame(anom_df)
    x,y = :TM,:SS
# @time    svgrid,keyvals,anomalies = SVGrid(df,data,x,y)
    anom_dysvgrid,anom_keyvals,anom_anomalies = DynamicSVGrid(Dict(),x,y,0),[],[]
    nbins = 3
@time DynamicSVGrid!(anom_dysvgrid,anom_df,anom_data,x,y,anom_keyvals,anom_anomalies;xbins=nbins,ybins=nbins,xcell_size=xcell_size,ycell_size=ycell_size)
    ukeys = length(unique(anom_keyvals))
    done = false
    while done == false
        println("Resolving Data: Unique Keys: $ukeys")
        [resolve!(anom_dysvgrid.grid[k],anom_df,anom_data,anom_keyvals,anom_anomalies,x,y;prev_key="$(k[1])-$(k[2])",xbins=nbins,ybins=nbins) for k in keys(anom_dysvgrid.grid)]
        current_ukeys = length(unique(anom_keyvals))
        if ukeys == length(unique(anom_keyvals))
            done = true
        end
        ukeys = current_ukeys
    end

end

@testset "SVGrid w/ Random Data" begin
    # SpatialVote(df,data,x,y,grids,keys,anomalies)

    data2 = [c for c in eachcol(rand(6,1_000_000))] 
    data2 = convert(Array{Any},data2)
    df2 = copy(data2)
    to_ascii!(df2)
    @time AllCompressiveEncodingStats!(df2)
    # @time ces = AllCompressiveEncodingStats(df)
    data2 = [convert(Array,data2[i]) for i in 1:length(data2)]
    df2 = DataFrame(df2)
    grids,keyvals,anomalies = Dict(),[],[]
    x,y = :RM,:TS
    dysvgrid,keyvals,anomalies = DynamicSVGrid(Dict(),x,y,0),[],[]
    @time DynamicSVGrid!(dysvgrid,df,data,x,y,keyvals,anomalies;xbins=nbins,ybins=nbins,xcell_size=50,ycell_size=50)
    [resolve!(dysvgrid.grid[k],df,data,keyvals,anomalies,x,y;prev_key="$(k[1])-$(k[2])") for k in keys(dysvgrid.grid)]
    plot(dysvgrid)


end

@testset "gridspace" begin


    xrange = -99:34:989
    x = 43
    bin = gridspace(x,xrange)
    bin_slow = gridspace_slow(x,xrange)
    @test bin == bin_slow

    xrange = 0:17:255
    x = 254
    bin = gridspace(x,xrange)
    bin_slow = gridspace_slow(x,xrange)
    @test bin == bin_slow

    xrange = -255:17:0
    x = -1
    bin = gridspace(x,xrange)
    bin_slow = gridspace_slow(x,xrange)
    @test bin == bin_slow

    xrange = 0:17:255
    x = 2
    bin = gridspace(x,xrange)
    bin_slow = gridspace_slow(x,xrange)
    @test bin == bin_slow

    xrange = range(-199,456,length=1000)
    x = -199
    bin = gridspace(x,xrange)
    bin_slow = gridspace_slow(x,xrange)
    @test bin == bin_slow

    xrange = 8.0:0.06791464462717589:56.96645877619382
    x = 20
    bin = gridspace(x,xrange)
    bin_slow = gridspace_slow(x,xrange)
    @test bin == bin_slow

    xrange = range(48.47712819779683,48.54504284242401,length=17)
    x = 48.44
    bin = gridspace(x,xrange)
    @test bin == nothing


end


@testset "groups()" begin
    diag_test_grid = Diagonal([1,2,3,4,5,7]) |> Array
    xlist,ylist = which_rows_cols_populated(diag_test_grid)
    @test xlist == [1,1,1,1,1,1]
    @test ylist == [1,1,1,1,1,1]
    xcellstart,xcellend = calc_start_stop(xlist)
    @test xcellstart == [1]
    @test xcellend  == [6]
    ycellstart,ycellend = calc_start_stop(ylist)
    @test ycellstart == [1]
    @test ycellend  == [6]

    test_group = SVGroup(Interval{:closed,:open}(1,2),Interval{:closed,:open}(1,2),0:17:255,0:17:255,13,13)
    real_range = get_real_range(test_group)
    @test real_range == (xstart=0,xstop=17,ystart=0,ystop=17)
    test_group = SVGroup(Interval{:closed,:open}(1,16),Interval{:closed,:open}(1,16),0:17:255,0:17:255)
    real_range = get_real_range(test_group)
    @test real_range == (xstart=0,xstop=255,ystart=0,ystop=255)
    test_group = SVGroup(4:8,1:2,0:17:255,0:17:255)
    real_range = get_real_range(test_group)
    @test real_range == (xstart=68,xstop=136,ystart=0,ystop=17)

end

@testset "Diagonal SVGrid" begin
    xrange = 0:17:255
    yrange = 0:17:255
    svdict = Dict()
    for i=1:16
        key = keyval(i,i,(16,16))
        exemplar = [i]
        group = nothing
        x = Interval{:closed,:open}(17*(i-1),17i)
        y = Interval{:closed,:open}(17*(i-1),17i)
        cell = SVCell([i],nothing,x,y,i)
        svdict[key] = cell
    end
    svgrid = SVGrid([],svdict,Dict(),xrange,yrange)
    test_groups = groups(svgrid)
    groups!(svgrid)
    xbin = Interval{:closed,:open}(1,16)
    ybin = Interval{:closed,:open}(1,16)
    @test length(test_groups) == 1
    @test test_groups[1] == SVGroup(xbin,ybin,xrange,yrange,12,12)
end

@testset "One Group" begin
    xsize,ysize = 16,16
    test_grid = reshape(collect(1:1:xsize*ysize),(xsize,ysize))
    lower_right_square = [191,207,223,239,238,222,206,190,189,205,221,237]
    test_grid[lower_right_square] .= 1
    test_grid

    xrange = 0:17:255
    yrange = 0:17:255
    svdict = Dict()
    for k=lower_right_square
        exemplar = ["Lower Right Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    svgrid = SVGrid([],svdict,Dict(),xrange,yrange)
    get_grid(svgrid)
    test_groups = groups(svgrid)
    @test length(test_groups) == 1
end

@testset "Two Group" begin
    xsize,ysize = 16,16
    test_grid = zeros(xsize,ysize)
    lower_right_square = [191,207,223,239,238,222,206,190,189,205,221,237]
    upper_left_square = [18,19,20,36,35,34,50,51,52,53,21,37]
    test_grid[lower_right_square] .= 1
    test_grid[upper_left_square] .= 1
    test_grid
    truth_xy = [
                (
                 x=Interval{:closed,:open}(2,5),y=Interval{:closed,:open}(2,6)
                )
                ,
                (
                 x=Interval{:closed,:open}(13,16),y=Interval{:closed,:open}(12,16)
                )
               ]

    xrange = 0:17:255
    yrange = 0:17:255
    svdict = Dict()
    for k=lower_right_square
        exemplar = ["Lower Right Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    for k=upper_left_square
        exemplar = ["Upper Left Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    svgrid = SVGrid([],svdict,Dict(),xrange,yrange)
    get_grid(svgrid)
    test_groups = groups(svgrid)
    grp = test_groups[1]
    @test sum(test_grid[grp.xbin.left:grp.xbin.right-1,grp.ybin.left:grp.xbin.right-1]) == length(upper_left_square)
    @test length(test_groups) == 2

end

@testset "Four Groups" begin
    xsize,ysize = 15,15
    # test_grid = reshape(collect(1:1:xsize*ysize),(xsize,ysize))
    test_grid = zeros(xsize,ysize)
    lower_left_square = [29,44,59,74,88,73,58,43,29,28,42,58,73,72,57,42,27,41]
    lower_right_square = [179,194,209,208,193,178,164,162,177,192,207]
    upper_right_square = [153,168,183,198,199,184,169,154,155,170,185,200,201,186,171,156]
    upper_left_square = [17,32,47,18,33,48,18,33,48,19,34,49]
    test_grid[lower_left_square] .= 1
    test_grid[lower_right_square] .= 1
    test_grid[upper_right_square] .= 1
    test_grid[upper_left_square] .= 1
    test_grid

    xrange = 0:17:255
    yrange = 0:17:255
    svdict = Dict()
    for k=lower_right_square
        exemplar = ["Lower Right Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    for k=lower_left_square
        exemplar = ["Lower Left Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    for k=upper_left_square
        exemplar = ["Upper Left Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    for k=upper_right_square
        exemplar = ["Upper Right Square"]
        group = nothing
        ix = CartesianIndices((xsize,ysize))[k]
        x,y = ix[1],ix[2]
        x = Interval{:closed,:open}(x,x+1)
        y = Interval{:closed,:open}(y,y+1)
        cell = SVCell(exemplar,nothing,x,y,k)
        svdict[k] = cell
    end
    svgrid = SVGrid([],svdict,Dict(),xrange,yrange)
    test_groups = groups(svgrid)
    groups!(svgrid)
    @test length(test_groups) == 4

    grid = get_grid(svgrid) |> Array
    group_grid = get_groups(svgrid) |> Array

    for g in 1:length(test_groups)
        grp = test_groups[g]
        for i=grp.xbin.left:grp.xbin.right-1, j=grp.ybin.left:grp.ybin.right-1
            grid[i,j] = g
        end
    end
end




Plots.Shape(w, h, x, y) = Plots.Shape(x .+ [0, w, w, 0, 0], y .+ [0, 0, h, h, 0])
box1 = Shape(10,10,5,5)
box2 = Shape(4,4,0,0)
plot([box1,box2])
